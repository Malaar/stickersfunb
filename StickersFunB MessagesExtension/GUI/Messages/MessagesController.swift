//
//  MessagesController.swift
//  StickersFunB MessagesExtension
//
//  Created by Malaar on 3/1/18.
//  Copyright © 2018 traitFactory. All rights reserved.
//

import UIKit
import Messages


//MARK: - MessagesController

class MessagesController: MSMessagesAppViewController {
    
    private var stickersController: StickersController!
    
    
    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        
        stickersController = StickersController()
        addChild(controller: stickersController)        
    }
    

    //MARK: - Manage child controller
    
    private func addChild(controller: UIViewController) {
        self.addChildViewController(controller)
        controller.view.frame = self.view.bounds
        self.view.addSubview(controller.view)
        controller.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        controller.didMove(toParentViewController: self)
    }
    
    private func removeChild(controller: UIViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
        controller.removeFromParentViewController()
    }
    
    
    //MARK: - Manage presentation style
    
    override func willResignActive(with conversation: MSConversation) {
        stickersController.willResignActive()
    }
    
    override func didTransition(to presentationStyle: MSMessagesAppPresentationStyle) {
        super.didTransition(to: presentationStyle)
        if presentationStyle == .compact {
            removeChild(controller: stickersController)
            addChild(controller: stickersController)
        }
    }
}
