//
//  StickersController.swift
//  StickersFunB MessagesExtension
//
//  Created by Malaar on 2/22/18.
//  Copyright © 2018 malaar. All rights reserved.
//

import UIKit
import Messages


//MARK: - Constants

fileprivate struct Constants {
    
    static let stickerResourceName = "stickers"
    static let stickerCellIdentifier = "stickerCell"
    static let footerIdentifier = "footer"
    
    struct CollectionLayout {
        
        static let footerHeight: CGFloat = 172.0
        struct Phone {
            static let itemMargin: CGFloat = 10.0
            static let columnsNumberPortrait = 3
            static let columnsNumberLandscape = 5
        }
        
        struct Pad {
            static let itemMargin: CGFloat = 20.0
            static let columnsNumberPortrait = 5
            static let columnsNumberLandscape = 6
        }
        
        static func itemMargin(traitCollection: UITraitCollection) -> CGFloat {
            return traitCollection.userInterfaceIdiom == .phone ? Phone.itemMargin : Pad.itemMargin
        }

        static func columnsNumber(traitCollection: UITraitCollection) -> Int {
            if traitCollection.userInterfaceIdiom == .phone {
                return UIScreen.main.isPortraitOrientation ? Phone.columnsNumberPortrait : Phone.columnsNumberLandscape
            } else {
                if traitCollection.horizontalSizeClass == .compact {
                    return Pad.columnsNumberPortrait
                } else {
                    return UIScreen.main.isPortraitOrientation ? Pad.columnsNumberPortrait : Pad.columnsNumberLandscape
                }
            }
        }
    }
}


//MARK: - StickersController

class StickersController: UICollectionViewController {

    private let stickersModelView: StickersModelView
    private var previousTraitCollection: UITraitCollection?
    private var lastPadOrientation: UIInterfaceOrientation = .unknown
    
    
    //MARK: - Initializers
    
    convenience init() {
        self.init(collectionViewLayout: UICollectionViewFlowLayout())
    }
    
    private override init(collectionViewLayout layout: UICollectionViewLayout) {
        let stickerProvider = StickerProviderJSON(json: Constants.stickerResourceName)
        self.stickersModelView = StickersModelView(stickerProvider: stickerProvider)
        super.init(collectionViewLayout: layout)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented. Don't use it")
    }
    
    
    //MARK: - Stickers management
    
    private func loadStickers () {
        stickersModelView.loadStickers { [weak self] in
            self?.collectionView?.reloadData()
            self?.loadContentOffset()
        }
    }
    
    
    //MARK: - Manage content offset
    
    private func saveContentOffset() {
        let offsetY = Double(self.collectionView!.contentOffset.y)
        stickersModelView.saveContentOffset(offsetY: offsetY)
    }
    
    private func loadContentOffset() {
        let offsetY = stickersModelView.loadContentOffset()
        self.collectionView?.performBatchUpdates(nil, completion: { _ in
            self.collectionView?.contentOffset = CGPoint(x: 0, y: offsetY)
        })
    }
    
    
    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        self.collectionView?.backgroundColor = UIColor.white
        self.collectionView?.contentInset = UIEdgeInsetsMake(Constants.CollectionLayout.itemMargin(traitCollection: self.traitCollection),
                                                             Constants.CollectionLayout.itemMargin(traitCollection: self.traitCollection),
                                                             0,
                                                             Constants.CollectionLayout.itemMargin(traitCollection: self.traitCollection))
        self.collectionView?.register(StickerCell.self, forCellWithReuseIdentifier: Constants.stickerCellIdentifier)
        let nib = UINib(nibName: "FooterView", bundle: nil)
        self.collectionView?.register(nib, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: Constants.footerIdentifier)
        
        loadStickers()
    }

    public func willResignActive() {
        saveContentOffset()
    }
    
    
    //MARK: - Trait collections
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        self.previousTraitCollection = previousTraitCollection
        self.collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if self.traitCollection.userInterfaceIdiom == .pad && self.traitCollection.containsTraits(in: self.previousTraitCollection){
            if UIScreen.main.isPortraitOrientation && lastPadOrientation != .portrait {
                lastPadOrientation = .portrait
                DispatchQueue.main.async {
                    self.collectionView?.collectionViewLayout.invalidateLayout()
                }
            } else if UIScreen.main.isLandscapeOrientation && lastPadOrientation != .landscapeLeft {
                lastPadOrientation = .landscapeLeft
                DispatchQueue.main.async {
                    self.collectionView?.collectionViewLayout.invalidateLayout()
                }
            }
        }
    }
}


//MARK: - UICollectionViewDataSource

extension StickersController {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return stickersModelView.stickersCount()
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.stickerCellIdentifier , for: indexPath) as! StickerCell
        cell.stickerView.sticker = stickersModelView.sticker(indexPath: indexPath)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Constants.footerIdentifier, for: indexPath)
        return view
    }
}


//MARK: - UICollectionViewDelegateFlowLayout

extension StickersController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width, height: Constants.CollectionLayout.footerHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let columnsNumber = Constants.CollectionLayout.columnsNumber(traitCollection: self.traitCollection)
        
        var margin = collectionView.contentInset.left + collectionView.contentInset.right
        if #available(iOSApplicationExtension 11.0, *) {
            margin += collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right
        } else {
            margin += collectionView.layoutMargins.left + collectionView.layoutMargins.right
        }
        margin += Constants.CollectionLayout.itemMargin(traitCollection: self.traitCollection) * (CGFloat(columnsNumber) - 1)
        
        var collectionViewSize = collectionView.bounds.size
        collectionViewSize.width = ((collectionViewSize.width - margin) / CGFloat(columnsNumber)).rounded(.down)
        collectionViewSize.height = collectionViewSize.width
        return collectionViewSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.CollectionLayout.itemMargin(traitCollection: self.traitCollection)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.CollectionLayout.itemMargin(traitCollection: self.traitCollection)
    }
}
