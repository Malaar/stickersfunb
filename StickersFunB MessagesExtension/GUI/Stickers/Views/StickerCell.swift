//
//  StickerCell.swift
//  StickersFunB MessagesExtension
//
//  Created by Malaar on 2/22/18.
//  Copyright © 2018 malaar. All rights reserved.
//

import UIKit
import Messages


//MARK: - StickerCell

class StickerCell: UICollectionViewCell {
 
    let stickerView = MSStickerView()
 
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentView.addSubview(stickerView)
        stickerView.translatesAutoresizingMaskIntoConstraints = false
        stickerView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor).isActive = true
        stickerView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor).isActive = true
        stickerView.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        stickerView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented. Don't use it")
    }
}
