//
//  UIScreen+Orientation.swift
//  StickersFunB MessagesExtension
//
//  Created by Malaar on 3/2/18.
//  Copyright © 2018 traitFactory. All rights reserved.
//

import UIKit


//MARK: - UIScreen orientation extension

extension UIScreen {
    
    public var orientation: UIInterfaceOrientation {
        let point = coordinateSpace.convert(CGPoint.zero, to: fixedCoordinateSpace)
        switch (point.x, point.y) {
        case (0, 0):
            return .portrait
        case let (x, y) where x != 0 && y != 0:
            return .portraitUpsideDown
        case let (0, y) where y != 0:
            return .landscapeLeft
        case let (x, 0) where x != 0:
            return .landscapeRight
        default:
            return .unknown
        }
    }
    
    public var isPortraitOrientation: Bool {
        return self.orientation == .portrait || self.orientation == .portraitUpsideDown
    }
    
    public var isLandscapeOrientation: Bool {
        return self.orientation == .landscapeLeft || self.orientation == .landscapeRight
    }
}

