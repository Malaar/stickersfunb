//
//  StickersModelView.swift
//  StickersFunB MessagesExtension
//
//  Created by Malaar on 3/21/18.
//  Copyright © 2018 traitFactory. All rights reserved.
//

import Foundation
import Messages


//MARK: - Constants

fileprivate struct Constants {
    static let stickersContentOffsetKey = "stickersContentOffset"
}


//MARK: - StickersModelView

class StickersModelView {
    
    private let stickerProvider: StickerProvider
    private var stickers = [MSSticker]()
    
    init(stickerProvider: StickerProvider) {
        self.stickerProvider = stickerProvider
    }

    public func loadStickers(callback: @escaping () -> Void) {
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            do {
                self?.stickers = (try self?.stickerProvider.loadStickers())!
            } catch {
                assertionFailure("Error stickers loading")
            }
            DispatchQueue.main.async {
                callback()
            }
        }
    }
    
    public func stickersCount() -> Int {
        return stickers.count
    }
    
    public func sticker(indexPath: IndexPath) -> MSSticker {
        return stickers[indexPath.row]
    }
    
    public func saveContentOffset(offsetY: Double) {
        UserDefaults.standard.set(offsetY, forKey: Constants.stickersContentOffsetKey)
        UserDefaults.standard.synchronize()
    }
    
    public func loadContentOffset() -> Double {
        let offsetY = UserDefaults.standard.double(forKey: Constants.stickersContentOffsetKey)
        return offsetY
    }
}
