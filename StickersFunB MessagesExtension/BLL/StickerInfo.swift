//
//  StickerInfo.swift
//  StickersFunB MessagesExtension
//
//  Created by Malaar on 3/1/18.
//  Copyright © 2018 traitFactory. All rights reserved.
//

import Foundation
import Messages


//MARK: - StickerInfo

class StickerInfo: Codable {
    
    let fileName: String
    let description: String
    
    init(fileName: String, description: String = "") {
        self.fileName = fileName
        self.description = description
    }

    public func createSticker() -> MSSticker? {
        guard let filePath = Bundle.main.path(forResource: fileName, ofType: nil) else {
            return nil
        }
        
        let stickerUrl = URL(fileURLWithPath: filePath)
        let sticker = try? MSSticker(contentsOfFileURL: stickerUrl, localizedDescription: description)
        return sticker
    }
}
