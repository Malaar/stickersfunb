//
//  StickerProvider.swift
//  StickersFunB MessagesExtension
//
//  Created by Malaar on 3/25/18.
//  Copyright © 2018 traitFactory. All rights reserved.
//

import Messages


//MARK: - StickerProvider

protocol StickerProvider {
    func loadStickers() throws -> [MSSticker]
}
