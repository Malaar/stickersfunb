//
//  StickerProvider.swift
//  StickersFunB MessagesExtension
//
//  Created by Malaar on 3/1/18.
//  Copyright © 2018 traitFactory. All rights reserved.
//

import Foundation
import Messages


//MARK: - StickerProviderError

enum StickerProviderError: Error {
    case fileNotFound
    case stickerWasntCreated
}


//MARK: - StickerProviderJSON

final class StickerProviderJSON: StickerProvider {
    
    private let resourceFileName: String
    
    init(json fileName: String) {
        self.resourceFileName = fileName
    }
    
    private func loadStickerInfos() throws -> [StickerInfo] {
        guard let filePath = Bundle.main.path(forResource: resourceFileName, ofType: "json") else {
            throw StickerProviderError.fileNotFound
        }
        let fileURL = URL(fileURLWithPath: filePath)
        let data = try Data(contentsOf: fileURL, options: .mappedIfSafe)
        let decoder = JSONDecoder()
        let stickerInfos = try decoder.decode([StickerInfo].self, from: data)
        return stickerInfos
    }
}


//MARK: - StickerProvider implementation

extension StickerProviderJSON {
    
    public func loadStickers() throws -> [MSSticker] {
        var stickers = [MSSticker]()
        let infos = try loadStickerInfos()
        for info in infos {
            if let sticker = info.createSticker() {
                stickers.append(sticker)
            } else {
                throw StickerProviderError.stickerWasntCreated
            }
        }
        return stickers
    }
}
